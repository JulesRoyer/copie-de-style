void mirroir(Capture video, PImage videoMirroir) {//Passe la video en mirroir
  int rouge, vert, bleu;
  color temp;
  video.loadPixels();
  videoMirroir.loadPixels();

  int[] maxI = {-1, -1, -1}, minI = {256, 256, 256};
  float[] val = {0.0, 0.0, 0.0};
  
  for (int yloc = 0; yloc < video.height; yloc++) {
    int rowPos = yloc * video.width;
    for (int xloc = 0; xloc < video.width; xloc++) {
      int loc1 = xloc + rowPos;
      int loc2 = video.width-1 - xloc + rowPos;
      
      temp = video.pixels[loc1];
      
      videoMirroir.pixels[loc2] = temp;
      
      rouge  = (int)red(temp);
      vert   = (int)green(temp);
      bleu   = (int)blue(temp);
      

      //Utile pour la suite
      maxI[0] = (rouge > maxI[0]) ? rouge : maxI[0];
      minI[0] = (rouge < minI[0]) ? rouge : minI[0];
      
      maxI[1] = (vert > maxI[1]) ? vert : maxI[1];
      minI[1] = (vert < minI[1]) ? vert : minI[1];
      
      maxI[2] = (bleu > maxI[2]) ? bleu : maxI[2];
      minI[2] = (bleu < minI[2]) ? bleu : minI[2];
    }
  }
  video.updatePixels();
  
  //on augmente le contraste par étirement de l'histogramme
  for (int i = 0; i<videoMirroir.height*videoMirroir.width; i++) {
    val[0] = (red(videoMirroir.pixels[i]) - minI[0]) / (maxI[0] - minI[0]);
    val[1] = (green(videoMirroir.pixels[i]) - minI[1]) / (maxI[1] - minI[1]);
    val[2] = (blue(videoMirroir.pixels[i]) - minI[2]) / (maxI[2] - minI[2]);
    videoMirroir.pixels[i] = color(contraste*val[0]*255 + luminosite, contraste*val[1]*255 + luminosite, contraste*val[2]*255 + luminosite);
  }
  videoMirroir.updatePixels();
}

//Uitle pour débugger
void tabCToPImage(color[][] a, PImage b){
  int k     = 0;
  for (int i=0; i<a.length; i++) {
    for (int j=0; j<a[0].length; j++) {
      b.pixels[k]=a[i][j];
      k++;
    }
  }
}

void imageTab(PImage depart, color[][] arrivee) {//Convertit une image PImage en matrice de int
  int k = 0;
  for (int i = 0; i < depart.height; i++) {
    for (int j = 0; j < depart.width; j++) {
      arrivee[i][j] = depart.pixels[k];
      k++;
    }
  }
}

void afficheImageC(color[][] a) {
  PImage b  = new PImage(a[0].length, a.length);
  tabCToPImage(a, b);
  image(b, 0, 0);
}

void enregistreImageC(color[][] a, String nom) {
  PImage b  = new PImage(a[0].length, a.length);
  tabCToPImage(a, b);
  b.save(nom);
}

void afficheImage(int[][] a) {
  PImage b  = new PImage(a[0].length, a.length);
  int k     = 0;
  for (int i=0; i<a.length; i++) {
    for (int j=0; j<a[0].length; j++) {
      b.pixels[k]=color(a[i][j], a[i][j], a[i][j]);
      k++;
    }
  }
  image(b, 0, 0);
}

void afficheBoutons(ArrayList<Bouton> b){
  int tailleL = b.size();
  int X, Y, L, H;
  color couleur;
  boolean sel;
  int num;
  String text;
  rectMode(CORNER);
  strokeWeight(tailleTrait);
  for(int i = 0; i<tailleL; ++i){
    X       = b.get(i).getX();
    Y       = b.get(i).getY();
    L       = b.get(i).getL();
    H       = b.get(i).getH();
    couleur = b.get(i).getC();
    sel     = b.get(i).getSel();
    num     = b.get(i).getNum();
    stroke(b.get(i).getC());
    if(sel) fill(color(50, 140, 230));
    else fill(240);
    
    if(num <= nombreStyles/2) {
      if(b.get(i).isOver(mouseX, mouseY)) X = X + 4;
    
      rect(X, Y, L, H, 0, 3, 3, 0);
    } else {
      if(b.get(i).isOver(mouseX, mouseY)) X = X - 4;
    
      rect(X, Y, L, H, 3, 0, 0, 3);
    }
    fill(couleur);
    textFont(nosNums);
    textSize(20);
    text = ""+ num;
    text(text, X + L/2 - textWidth(text)/2, Y + H/2 + 20/2);
    noFill();
  }
  rectMode(CENTER);
}

void changeCouleur(ArrayList<Bouton> b, int x){
  color debase = color(15);
  color plus = color(204,25,25);
  for(int i = 0; i < nombreStyles; ++i){
    if(i==x){
      b.get(i).setC(plus);
      b.get(i).setSel(true);
    }
    else {
      b.get(i).setC(debase);
      b.get(i).setSel(false);
    }
  }
  
}
