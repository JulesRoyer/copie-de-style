//Si la personne ne bouge pas trop, on enregistre sa position et on garde l'image
void prendrePhoto() {
  int varianceMax = 20;
  int moy = 0;
  int moyCarre = 0;
  int variance;
  int[] distance = new int[coords.size()-1];
  int tailleD = distance.length;

  for (int i=1; i<coords.size(); i++) {
    //Distance de l'échiquier
    distance[i-1] = max(abs(coords.get(0)[0]-coords.get(i)[0]), abs(coords.get(0)[1]-coords.get(i)[1]));
  }
  for (int i=0; i<tailleD; i++) {
    moy += distance[i];
    moyCarre += distance[i]*distance[i];
  }
  moy /= tailleD;
  moyCarre /= tailleD;

  variance = moyCarre - (moy*moy);

  if (variance<=varianceMax) {
    //on "prend" une photo;
    photo = videoMirroir.copy();
    etat = 2;
  }
}

//Dessine des carrés autour des visages détectés 
void trouveVisage() {
  //On détecte les visages avec la bibliothèque OpenCV
  f = opencv.detect();
  stroke(255, 0, 0);
  strokeWeight(3);
  
  //Si on trouve des visages
  //On dessine les carrés correspondants
  if (f.length>0) {
    //On cherche le visage qui a la plus grande taille
    int maxAire = 0;
    int indiceBon = 0;
    for (int i = 0; i<f.length; i++) {
      int aire = f[i].height*f[i].width;
      if (aire>maxAire) {
        maxAire = aire;
        indiceBon = i;
      }
    }

    int Xm = f[indiceBon].x+(f[indiceBon].width/2);
    int Ym = f[indiceBon].y+(f[indiceBon].height/2);
    
    if (Xm>videoMirroir.width/3 && Xm<2*videoMirroir.width/3 
      && Ym>videoMirroir.height/3 && Ym<2*videoMirroir.height/3) {
      //On redimensionne proportionnellement à la taille d'origine
      //Seulement si le visae detecté est dans le tier du milieu
      //Pour prendre un rectangle plus grand (sinon certaines partie
      //du crane sont coupées
      dernierVisage = f[indiceBon];

      int Y = dernierVisage.y;

      int coeffHaut = (int)(dernierVisage.height);
      dernierVisage.y = (dernierVisage.y > coeffHaut/2) ? dernierVisage.y-coeffHaut/2 : 0;
      dernierVisage.height = dernierVisage.height + (Y-dernierVisage.y);
      dernierVisage.height += (dernierVisage.y + dernierVisage.height + coeffHaut/2 < videoMirroir.height) ? coeffHaut/2 : videoMirroir.height - (dernierVisage.y + dernierVisage.height);

      int X = dernierVisage.x;//Pareil

      int coeffLarge = (int)(dernierVisage.width/5);
      dernierVisage.x = (dernierVisage.x > coeffLarge/2) ? dernierVisage.x-coeffLarge/2 : 0;
      dernierVisage.width = dernierVisage.width + (X-dernierVisage.x);
      dernierVisage.width += (dernierVisage.x + dernierVisage.width +coeffLarge/2 < videoMirroir.width) ? coeffLarge/2 : videoMirroir.width - (dernierVisage.x + dernierVisage.width);
      
      int[] tab = {Xm, Ym};
      coords.add(tab);

      if (coords.size() > 10) {//On dessine le rectangle autour du visage sélectionné
        rect(f[indiceBon].x+f[indiceBon].width/2, f[indiceBon].y+f[indiceBon].height/2, f[indiceBon].width, f[indiceBon].height);
        fill(204,25,30);
        textSize(17);
        text("Attention, ne bougez plus !", 15, 30);
        noFill();
      }
      if (coords.size() > 20) {
        coords.remove(0);
        
        prendrePhoto();
      }
    }
  }
}
