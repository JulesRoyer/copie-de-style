#!/bin/bash

python style.py --style ./a_entrainer/dali.jpg --checkpoint-dir ./check/
python style.py --style ./a_entrainer/delaunay1.jpg --checkpoint-dir ./ckptDelau/ --test ./test/stata.jpg --test-dir ./test/ --checkpoint-iterations 1000 --batch-size 2