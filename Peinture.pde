

/*-------------------- Peinture = portrait + style utilisé --------------------*/
public class Peinture{
  private PImage artwork;
  private PImage resultat;
  
  public Peinture(PImage Art, PImage Res){
    this.artwork = Art.copy();
    this.resultat = Res.copy();
  }
  
  public PImage art(){
    return this.artwork;
  }
  
  public PImage res(){
    return this.resultat;
  }
}



/*-------------------- Boutons pour l'affichage --------------------*/
public class Bouton{
  private int x;
  private int y;
  private int h;
  private int l;
  private int n;
  private color c;
  private boolean select;
  
  public Bouton(int x, int y, int larg, int haut, int num){
    this.x = x;
    this.y = y;
    this.h = haut;
    this.l = larg;
    this.n = num;
    this.c = color(15);
    this.select = false;
  }
  
  public boolean isOver(int mx, int my){
    return (mx/scale > this.x && mx/scale < this.x + this.l && my/scale > this.y && my/scale < this.y + this.h);
  }
  
  public color getC(){
    return this.c;
  }
  
  public void setC(color a){
    this.c = a;
  }
  
  public boolean getSel(){
    return this.select;
  }
  
  public void setSel(boolean a){
    this.select = a;
  }
  
  public int getX(){
    return this.x;
  }
  
  public int getY(){
    return this.y;
  }
  
  public int getL(){
    return this.l;
  }
  
  public int getH(){
    return this.h;
  }
  
  public int getNum(){
    return this.n;
  }
}
