# Projet de ZZ1 (1ère année ISIMA)
Copie de style par réseau de neurones adaptée à Processing basé sur le projet Lengstrom

## Utilisation
### Principe

### Résultats

## Pré-requis
### Python 2.7 ou 3.6

### numpy, scipy et pillow

### Tensorflow

### Processing

## Reste de la documentation (en anglais)
[Documentation du projet Lengstrom ici](https://github.com/lengstrom/fast-style-transfer)