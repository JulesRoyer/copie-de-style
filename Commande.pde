import java.awt.*;
import java.awt.event.*;

public class BoutonCroix implements WindowListener {  
  @Override
    public void windowClosing(WindowEvent e) {
    ((Frame)e.getSource()).dispose();
    flagComm = false;
  }

  @Override
    public void windowActivated(WindowEvent e) {
  }
  @Override
    public void windowClosed(WindowEvent e) {
  }
  @Override
    public void windowDeactivated(WindowEvent e) {
  }
  @Override
    public void windowDeiconified(WindowEvent e) {
  }
  @Override
    public void windowIconified(WindowEvent e) {
  }
  @Override
    public void windowOpened(WindowEvent e) {
  }
}


public class Commande extends Frame implements ActionListener {
  private Frame com = new Frame("Commandes");
  private BoutonCroix cr = new BoutonCroix();

  private Button rst = new Button("Reset (espace)");
  private Button enreg = new Button("Sauvegarder les résultats (entree)");
  private Button sauv = new Button("Revenir au diaporama (t)");

  private Label nivCont = new Label("Niveau de contraste");
  private Button contP = new Button("+ (UP)");
  private Button contM = new Button("- (DOWN)");

  private Label nivLum = new Label("Niveau de luminosité");
  private Button lumP = new Button("+ (RIGHT)");
  private Button lumM = new Button("- (LEFT)");

  private Panel rds = new Panel();
  private Panel panCont = new Panel();
  private Panel panLum = new Panel();

  public Commande() {
    com.setSize(500, 500);
    com.setVisible(true);
    com.addWindowListener(cr);

    com.setLayout(new BorderLayout());
    rds.setLayout(new BorderLayout());
    panCont.setLayout(new BorderLayout());
    panLum.setLayout(new BorderLayout());

    rds.add(rst, BorderLayout.NORTH);
    rds.add(enreg, BorderLayout.CENTER);
    rds.add(sauv, BorderLayout.SOUTH);
    com.add(rds, BorderLayout.NORTH);

    panLum.add(nivLum, BorderLayout.NORTH);
    panLum.add(lumM, BorderLayout.WEST);
    panLum.add(lumP, BorderLayout.EAST);
    com.add(panLum, BorderLayout.WEST);

    panCont.add(nivCont, BorderLayout.NORTH);
    panCont.add(contM, BorderLayout.WEST);
    panCont.add(contP, BorderLayout.EAST);
    com.add(panCont, BorderLayout.EAST);

    rst.addActionListener(this);
    enreg.addActionListener(this);
    sauv.addActionListener(this);
    contM.addActionListener(this);
    contP.addActionListener(this);
    lumM.addActionListener(this);
    lumP.addActionListener(this);

    com.pack();
  }

  @Override
    public void actionPerformed(ActionEvent e) {
    if (e.getSource() == rst) {
      etat = 1;
      defile = true;
      nom = 0;
      coords.clear();
    }

    if (e.getSource() == contP) {
      contraste += 0.1;
    }
    if (e.getSource() == contM) {
      contraste -= (contraste<0.1) ? 0 : 0.1;
    }
    if (e.getSource() == lumP) {
      luminosite += 5;
    }
    if (e.getSource() == lumM) {
      luminosite -= 5;
    }
    if (e.getSource() == enreg) {
      enregistrer();
    }
    if (e.getSource() == sauv) {
      etat=4;
    }
  }
}
