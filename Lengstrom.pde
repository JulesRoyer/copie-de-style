import gab.opencv.*;
import processing.video.*;
import java.awt.*;
import java.util.*;
import javax.swing.*;

//Video + affichage processing
Capture video;//Image de la webcam
OpenCV opencv;//Pour détecter les visages
PImage logo;//Logo de l'isima
PImage videoMirroir;//Webcam en mirroir
PImage photo;
PImage portrait;
PImage stayle;

//Images
color[][] visageDetecte;

ArrayList<int[]> coords   = new ArrayList<int[]>();
ArrayList<Peinture> menu  = new ArrayList<Peinture>();
ArrayList<Bouton> bota    = new ArrayList<Bouton>();

ProcessBuilder pb;
Process p;
ProcessBuilder sauvegarde;
Process sauver;
String python;
String systeme;

Rectangle[] f;//Pour stocker les rectangles des visages détectés
Rectangle dernierVisage;

//Variables pour les differentes etapes
int etat           = 1;
boolean flagComm   = true;
boolean defile     = true;

//permet une boite de dialogue pour choisir l'emplacement de l'enregistrement de l'affichage du logiciel
JFileChooser choix = new JFileChooser();

int nom   = 0;
int frame = 0;
int nombreStyles;
File[] styles;
int taille;
int tailleTrait = 2;

PFont nosNoms;
PFont nosNums;
float contraste  = 1.0;
int luminosite   = 0;

float scale = 1.2;
int l = int(640*scale);
int h = int(480*scale);

long tempsDepart;
long tempsPendant;
long tempsFrame = 1500;//=1,5s


void settings() {
  size(l, h, P2D);
}

void setup() {
  surface.setTitle("Projet portraits");
  noFill();
  rectMode(CENTER);
  colorMode(RGB);
  noSmooth();

  //ProcessBuilder ==> Permet de lancer des actions en ligne de commande
  pb = new ProcessBuilder();//.inheritIO();//Utile pour débugger
  pb.directory(new File(sketchPath()));//Pour que le PB sache ou il travaille
  sauvegarde = new ProcessBuilder();//.inheritIO();
  sauvegarde.directory(new File(sketchPath()));//Pour que le PB sache ou il travaille
  
  //Detection du systeme et adaptation des lignes de commandes
  systeme = System.getProperty("os.name");
  if (systeme.contains("Windows")) {
    systeme = "Windows";
  }
  println(systeme);
  
  //Lancement processBuilder (suppression fichier (mac) indesirable puis lancement)
  switch(systeme) {
    case("Windows"):
    pb.command("CMD", "/C", "del .\\style\\.DS_Store");
    break;
  default : 
    pb.command("rm", "./style/.DS_Store");
    break;
  }
  try {
    p = pb.start();
    p.waitFor();
  }
  catch(Exception e) {
    println("erreur");
  }
  
  //Construit la liste des fichiers de styles disponibles
  styles = new File(sketchPath()+"/style/").listFiles();
  nombreStyles = styles.length;

  //User Interface pour la sauvegarde
  choix.setCurrentDirectory(new File("/")); 
  choix.changeToParentDirectory();
  choix.setDialogTitle("Choisissez un nom de dossier et son emplacement pour vos portraits :");

  nosNums = createFont("HAND.TTF", 64);
  nosNoms = createFont("handwriting-draft.ttf", 64);

  //webcam en mirroir
  video = new Capture(this, 640, 480, 30);
  videoMirroir = createImage(video.width, video.height, RGB);

  logo = loadImage("logo_isima_uca.png");

  //Reconnaissance de visage
  opencv = new OpenCV(this, video.width, video.height);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);

  //demarrage webcam
  video.start();

  //Fenetre de commande
  new Commande();

  logo.resize(0, 25);

  //taille des boutons
  taille = ((int(2*h/scale) - logo.height) / (nombreStyles)) - (tailleTrait + 3);
  
  python = "/Library/Frameworks/Python.framework/Versions/2.7/bin/python";//Emplacement de python pour Mac

  println("\n\n\n\n\nAppuyez sur ESPACE pour reprendre une photo,\nAppuyez sur ENTREE pour enregister les portraits stylisés");
  println("Appuyez sur 'c' pour rouvrir à nouveau la fenêtre de contrôle");

  
  //C'est parti !
  switch(systeme) {
    case("Windows"):
    pb.command("CMD", "/C", "echo C'est parti !");
    break;
  default: 
    pb.command("echo", "C'est parti !");
    break;
  }
  try {
    p = pb.start();
    p.waitFor();
  }
  catch(Exception e) {
    println("erreur");
  }

  //fin du setup
}




void draw() {
  scale(scale);
  textFont(nosNoms);

  //Etape 1 --> Trouver un visage qui ne bouge pas et qui soit le plus grand
  if (video.available() && etat == 1) {//On détecte les visages et si la personne ne bouge pas on la prend en photo pour ensuite travailler sur une image fixe

    video.read();

    mirroir(video, videoMirroir);

    //Caméra en mirroir
    image(videoMirroir, 0, 0);

    opencv.loadImage(videoMirroir);

    //On dessine alors le visage détecté au bout d'un certain temps
    trouveVisage();

    //Etape 2 --> Le sauvegarder pour détecter tous les contours
  } else if (video.available() && etat == 2) {
    Rectangle travail = dernierVisage;//On recopie le portrait detecté dans une matrice
    //pour ne travailler ensuite que sur ce portrait
    visageDetecte = new color[travail.height][travail.width];

    photo.loadPixels();

    for (int i = 0; i < travail.height; i++) {
      int pos = (i+travail.y)*photo.width;
      for (int j = 0; j < travail.width; j++) {
        int pos2 = travail.x + pos + j;
        visageDetecte[i][j] = photo.pixels[pos2];
      }
    }


    photo.updatePixels();

    //afficheImageC(visageDetecte);//débuggage

    etat = 3;

    enregistreImageC(visageDetecte, sketchPath()+"/resultats/portrait.jpg");

    background(255);


    /********************************************************************* Affichage ***********************************************************************/
    //Etape 3 --> On dessine tous les styles de protraits trouvés
  } else if (nom < nombreStyles && etat == 3) {
    background(255);
    String nous = "Chargement en cours...";

    if (!p.isAlive()) {
      if (nom > 0) {//Affichage petit à petit des nouvelles images
        background(255);

        stayle = loadImage("./style/"+styles[frame].getName()+"/art.jpg");
        portrait = loadImage("./resultats/res"+frame+".jpg");

        image(stayle, width/4/scale - stayle.width/2, height/2/scale - stayle.height/2);
        image(portrait, 3*width/4/scale - portrait.width/2 - 35, height/2/scale - portrait.height/2);
      }

      switch (systeme) {
        case ("Windows"):
        pb.command("CMD", "/C", "python evaluate.py "
          +"--checkpoint .\\style\\"+styles[nom].getName()+"\\style.ckpt "
          + "--in-path .\\resultats\\portrait.jpg "
          + "--out-path .\\resultats\\res"+nom+".jpg");
        break;
        case("Mac OS X"):   
        pb.command(python, 
          "evaluate.py", 
          "--checkpoint", 
          "./style/"+styles[nom].getName()+"/style.ckpt", 
          "--in-path", 
          "./resultats/portrait.jpg", 
          "--out-path", 
          "./resultats/res"+nom+".jpg");
        break;
        default:   
        pb.command("python2.7", 
          "evaluate.py", 
          "--checkpoint", 
          "./style/"+styles[nom].getName()+"/style.ckpt", 
          "--in-path", 
          "./resultats/portrait.jpg", 
          "--out-path", 
          "./resultats/res"+nom+".jpg");
        break;
      }
      frame = nom;
      nom++;
      try {
        p = pb.start();
        p.waitFor();
      }
      catch(Exception e) {
        println(e);
        exit();
      }
    }
    
    fill(255);
    noStroke();
    rectMode(CORNER);
    textSize(20);//textWidth dépend de la taille de la police, il faut donc la changer avant les calculs
    rect(width/2/scale - (textWidth(nous) + 5)/2, (2*height/3 - 20)/scale - 2, (textWidth(nous) + 5), 25, 7);
    fill(200);
    rect(width/2/scale - (textWidth(nous) + 5)/2 + 2, (2*height/3 - 20)/scale, frame*((textWidth(nous) + 5)/nombreStyles) - 4, 20, 5);
    fill(204,25,30);
    text(nous, width/2/scale - textWidth(nous)/2, 2*height/3/scale);
    rectMode(CENTER);
    
    if(nom >= nombreStyles) {
      etat = 4;
    }
    
  } else if (etat == 4 && (new File(sketchPath()+"/resultats/").listFiles().length) >= nombreStyles) {//On génère tous les Bouton et Peinture associes au styles
    background(255);
    menu.clear();
    bota.clear();
    for (frame = 0; frame < nombreStyles; ++frame) { 
      stayle = loadImage("./style/"+styles[frame].getName()+"/art.jpg");
      portrait = loadImage("./resultats/res"+frame+".jpg");

      menu.add(new Peinture(stayle, portrait));
      if(frame < (nombreStyles/2)) {
        bota.add(new Bouton(-1, 2+frame*(taille+tailleTrait+3), 35, taille, frame + 1));
      }
      else {
        bota.add(new Bouton(int(l/scale - 34), 2+(frame - nombreStyles/2)*(taille+tailleTrait+3), 35, taille, frame + 1));
      }
    }
    frame = 0;
    tempsDepart = System.currentTimeMillis();
    etat = 5;
    
  } else if (etat == 5 && bota.size() > 0){ //On affiche un diaporama
    background(255);

    portrait = menu.get(frame).res();//résultat
    stayle = menu.get(frame).art();//artwork pour comparaison
    
    image(stayle, width/4/scale - stayle.width/2, height/2/scale - stayle.height/2);
    image(portrait, 3*width/4/scale - portrait.width/2 - 35, height/2/scale - portrait.height/2);

    changeCouleur(bota, frame); //

    if (defile) {//Affichage toutes les 1,5 secondes de l'image stylisee suivante
      tempsPendant = System.currentTimeMillis();
      if (tempsPendant > tempsDepart + tempsFrame) {
        tempsDepart = System.currentTimeMillis();
        frame = (frame + 1) % (nombreStyles);
      }
    }

    afficheBoutons(bota);
  }
  //Dans tout les cas :
  //Logo isima
  image(logo, 0, (height/scale)-logo.height);
}

void enregistrer(){ 
  if (bota.size() == nombreStyles) {//On vérifie bien que les styles ont été évalués sur le portrait
    choix.showSaveDialog(null);
    File ou = choix.getSelectedFile();
    if (ou != null) {
      String sauv = ou.getAbsolutePath();
      switch (systeme) {
        case("Windows"):
        sauvegarde.command("CMD", "/C", "mkdir "+sauv);
        try {
          sauvegarde.start().waitFor();
        }
        catch(Exception e) {
          println(e);
        }
        sauvegarde.command("CMD", "/C", "copy .\resultats " + sauv);
        break;
        default:
        sauvegarde.command("cp", "-R", "./resultats", sauv);
        break;
      }
      try{
        sauver = sauvegarde.start();
        sauver.waitFor();
        println("Enregistré à " + sauv);
      }
      catch(Exception e) {
        println(e);
        println("Erreur à l'enregistrement");
      }
    } else {
      println("Vous avez annulé... ou une erreur s'est produite");
    }
  }
}

void mousePressed() {
  boolean k = false;
  if (bota.size() == nombreStyles) {//on a bien affiché tous les boutons
    for (int i=0; i<nombreStyles; ++i) {
        if (bota.get(i).isOver(mouseX, mouseY)) {
          k = true;
          frame = i;
        }
      }
      if (k) {
        defile = false;
      }
      else {
        defile = true;
        tempsDepart = System.currentTimeMillis();
      }

  }
}

void keyPressed() {
  switch(key) {
    case(' '): // Réinitialisation du programme
    etat = 1;
    defile = true;
    nom = 0;
    coords.clear();
    break;
    case('t'):
    case('T'):
    etat=3;
    break;
    case('c'):
    case('C'):
    if (!flagComm) {
      flagComm = true;
      new Commande();
    }
  }
  switch(keyCode) {
    case(UP):
    contraste += 0.1;
    break;
    case(DOWN):
    contraste -= (contraste < 0.1) ? 0.0 : 0.1;
    break;
    case(RIGHT):
    luminosite += 5;
    break;
    case(LEFT):
    luminosite -= 5;
    break;
    case(ENTER):
    enregistrer();
    break;
  }
}
